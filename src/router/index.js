import Vue from 'vue'
import VueRouter from 'vue-router'

// 基础
import Login from '../components/Login.vue'
import Home from '../components/Home.vue'
import Welcome from '../components/Welcome.vue'

// 项目管理
import DepartmentList from '../components/department/List.vue'
import Env from '../components/env/List.vue'
import Project from '../components/project/List.vue'
import ProjectSystem from '../components/project/ProjectSystem.vue'
import Version from '../components/project/Version.vue'

// 报告管理
import JiradcHome from '../components/jiradc/Jiradc.vue'
import ReportProgress from '../components/jiradc/Progress.vue'
import Daily from '../components/jiradc/Daily.vue'
import ZenTao from '../components/jiradc/ZenTao.vue'
import Jql from '../components/jiradc/Jql.vue'

// 任务管理
import MyTask from '../components/todo/MyTask.vue'
import ApiTask from '../components/todo/ApiTask.vue'
import TpTask from '../components/todo/TpTask.vue'
import ProjectTask from '../components/todo/ProjectTask.vue'

// mock管理
import Mock from '../components/mock/LIst.vue'
import MockInit from '../components/mock/Init.vue'

// 工具管理
import SoftTest from '../components/blog/SoftTest.vue'
import Python from '../components/blog/Python.vue'
import Json from '../components/tools/Json.vue'

// 博客管理
// 路由懒加载
// const Login = () => import('../components/Login.vue')
// const Home = () => import('../components/Home.vue')
// const Welcome = () => import('../components/Welcome.vue')
// const DepartmentList = () => import('../components/department/List.vue')

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/welcome',
    name: 'Welcome',
    component: Welcome
  },

  {
    path: '/home',
    name: 'home',
    component: Home,
    redirect: '/welcome',
    children: [
      { path: '/home', component: Welcome },
      // 项目管理
      { path: '/department', component: DepartmentList },
      { path: '/env', component: Env },
      { path: '/project', component: Project },
      { path: '/projectSystem', component: ProjectSystem },
      { path: '/version', component: Version },
      // 报告管理
      { path: '/jql', component: Jql },
      { path: '/progress', component: ReportProgress },
      { path: '/jiradc', component: JiradcHome },
      { path: '/zentao', component: ZenTao },
      { path: '/daily', component: Daily },
      // 任务管理
      { path: '/projecttask', component: ProjectTask },
      { path: '/mytask', component: MyTask },
      { path: '/apitask', component: ApiTask },
      { path: '/tptask', component: TpTask },
      // Mock管理
      { path: '/mockinit', component: MockInit },
      { path: '/mock', component: Mock },
      // 工具管理
      { path: '/softtest', component: SoftTest },
      // 博客管理
      { path: '/python', component: Python },
      { path: '/json', component: Json }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

// 挂载路由导航守卫,to表示将要访问的路径，from表示从哪里来，next是下一个要做的操作 next('/login')强制跳转login
router.beforeEach((to, from, next) => {
  // 访问登录页，放行
  if (to.path === '/login') return next()
  // 获取token
  const tokenStr = window.sessionStorage.getItem('token')
  // 没有token, 强制跳转到登录页
  if (!tokenStr) return next('/login')
  next()
})

export default router
