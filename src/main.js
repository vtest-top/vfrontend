import Vue from 'vue'
import App from './App.vue'
import router from './router'
// import moment from 'moment'
// import store from './store'
import './plugins/element.js'
// 导入全局样式
import './assets/css/global.css'
// 导入全局vue
import global_ from './Global.vue'
// 导入图标库
// import './assets/fonts/iconfont.css'
import './assets/iconfont/iconfont.css'
// 导入axios
import axios from 'axios'
// 导入NProgress, 包对应的JS和CSS
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'
Vue.use(mavonEditor)

// 远程后台地址
Vue.prototype.GLOBAL = global_
axios.defaults.baseURL = global_.BASE_URL
// axios.defaults.baseURL = 'http://10.179.67.88:12306'
// axios.defaults.baseURL = 'http://30.76.226.179:12306'

// 挂在到Vue实例，后面可通过this调用
Vue.prototype.$http = axios
// Vue.prototype.$base_url = 'http://10.179.67.88:8000/mockapi'

// Vue.prototype.$moment = moment
// 在request 拦截器中, 展示进度条 NProgress.start()
// 请求在到达服务器之前，先会调用use中的这个回调函数来添加请求头信息
axios.interceptors.request.use(config => {
  NProgress.start()
  // console.log(config)
  // 为请求头对象，添加token验证的Authorization字段
  config.headers.Authorization = window.sessionStorage.getItem('token')
  // 在最后必须 return config
  return config
})
// response 拦截器中,  隐藏进度条NProgress.done()
axios.interceptors.response.use(config => {
  NProgress.done()
  return config
})

Vue.config.productionTip = false

new Vue({
  router,
  // store,
  render: h => h(App)
}).$mount('#app')
